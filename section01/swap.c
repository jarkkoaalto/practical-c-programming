/*
 ============================================================================
 Name        : swap.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : swap two numbers without using third variable program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int a ,b ;
	printf("Enter value for a and b");
	scanf("%d%d", &a,&b);
	a = a+b;
	b = a-b;
	a = a-b;
	printf("After the swaping the value of a and b is: %d %d", a,b);
	return 0;
}
