/*
 ============================================================================
 Name        : SumTwoMatrix.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Sum of two matrix program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int a[3][2];
	int b[3][2];
	int c[3][2];
	int i,j;

	printf("Enter value of a matrix:");
	for(i=0;i<3;i++){
		for(j=0;j<2;j++){
			scanf("%d", &a[i][j]);
		}

	}
	printf("Enter value for b matrix:");
	for(i=0;i<3;i++){
		for(j=0;j<2;j++){
			scanf("%d", &b[i][j]);
		}
	}

	for(i=0;i<3;i++){
		for(j=0;j<2;j++){
			c[i][j] = a[i][j]+b[i][j];
		}
	}
	printf("Sum of matrix is\n");
	for(i=0;i<3;i++){
		for(j=0;j<2;j++){
			printf("%d\t",c[i][j]);
		}
		printf("\n");
	}


	return 0;
}

