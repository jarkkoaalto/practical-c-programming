/*
 ============================================================================
 Name        : MaximumNumberInArray.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Maximum Number In Arrayprogram
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
int a[5];
int i;
int max;
printf("Enter content for the Array:");
for(i=0;i<5;i++){
	scanf("%d", &a[i]);
}
max = a[0];

for(i=0;i<5;i++){
	if(max<a[i])
		max = a[i];
}
printf("Maximum number = %d", max);
	return 0;
}

