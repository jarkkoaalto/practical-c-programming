/*
 ============================================================================
 Name        : Concatenation.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Concate two string without using library program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	char string1[50];
	char string2[50];
	char i, j;

	printf("Enter first string:");
	scanf("%s",string1);
	printf("Enter second string:");
	scanf("%s",string2);

	for(i=0;string1[i]!='\0';i++);
	for(j=0;string2[i]!='\0';j++,i++)
	{
		string1[i] = string2[j];

	}
	string1[i] ='\0';
	printf("\n output: %s", string1);
	return 0;
}

