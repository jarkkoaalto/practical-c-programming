/*
 ============================================================================
 Name        : StringsLength.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Strings Length program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	char s[100];
	int i;
	printf("Enter string:\n");
	scanf("%s",s);

	for(i=0;s[i]!='\0';i++);
	printf("Length of string is: %d",i);


	return 0;
}


