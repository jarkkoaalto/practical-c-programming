/*
 ============================================================================
 Name        : Strings.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Count lower, upper and special characters program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int upper = 0, lower=0, others=0;
	char ch[80];
	int i;

	printf("\nEnter the String:\n");
	gets(ch);

	i=0;
	while(ch[i] != '\0')
	{
		if(ch[i] >= 'A' && ch[i] <= 'Z')
			upper++;
		if(ch[i] >= 'a' && ch[i] <= 'z')
			lower++;
		else
			others++;
		i++;
	}

	printf("\nUppercase Letters : %d", upper);
	printf("\nLowercase Letters : %d", lower);
	printf("\nOther Letters : %d", others);

	return 0;
}

