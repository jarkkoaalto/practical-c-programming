/*
 ============================================================================
 Name        : calculate_percentage.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : calculate percentage program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int a,b,c;

	printf("Enter the value a:\n");
	scanf("%d", &a);
	printf("Enter the value b:\n");
	scanf("%d", &b);
	printf("Enter the value c:\n");
	scanf("%d", &c);
	c = a+b;
	printf("The sum is: %d\n", c);
	return 0;
}
