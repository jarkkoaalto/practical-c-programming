/*
 ============================================================================
 Name        : AverageElementInArray.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Average Element In Array program
 ============================================================================
*/

#include<stdio.h>
#include<conio.h>

int main()
{
	int a[10];
	int i;
	int sum = 0;
	float avg;

	printf("Enter element of an Array");

	for(i=0;i<10;i++){
		scanf("%d", &a[i]);
	}
	for(i=0;i<10;i++){
		sum = sum+a[i];
	}
	printf("Sum = %d",sum);
	avg = (sum / 10);
	printf("Average = %f", avg);

	return 0;
}
