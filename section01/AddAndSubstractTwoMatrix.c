/*
 ============================================================================
 Name        : AddAndSubstractTwoMatrix.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add And Substract Two Matrix program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int a[3][3], b[3][3],c[3][3];
	int i,j,k,x;

	printf("\n\tMatrix\n\t1)Sum\n\t2)Difference\n\t3)Multiplication\n\t4)Transpose\n\tInplace transpose\n\6) Skew Symetric\n\tExit");
	scanf("%d", &x);

switch(x)
{
	case 1:
	{
		printf("Enter Two matrix \n");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				b[i][j]=0;
				a[i][j]=0;
				c[i][j]=0;
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d",&a[i][j]);
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d", &b[i][j]);
			}
		}

		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				c[i][j] = a[i][j]+b[i][j];
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				printf("%d\t", c[i][j]);
			}
			printf("\n");
		}
		printf("\nis the sum of the matrix.\nThank you!");
		break;
	}
	case 2:
		{
		printf("Enter two martrix\n");
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				{
				b[i][j]=0;
				a[i][j]=0;
				c[i][j]=0;
				}
			}
		for(i=0;i<3;i++)
				{
				for(j=0;j<3;j++)
					{
						scanf("%d",&a[i][j]);
					}
				}
		for(i=0;i<3;i++)
				{
				for(j=0;j<3;j++)
				{
					scanf("%d", &b[i][j]);
				}
		}
		for(i=0;i<3;i++)
			{
			for(j=0;j<3;j++)
			{
				c[i][j] = a[i][j]-b[i][j];
			}
		}
		for(i=0;i<3;i++)
			{
			for(j=0;j<3;j++)
			{
				printf("%d\t", c[i][j]);
			}
		printf("\n");
	}
	printf("\nis the difference of the matrix.\nThank you!");
	break;
	}

	case 3:
		{
		printf("Enter Two matrix \n");
			for(i=0;i<3;i++)
			{
			for(j=0;j<3;j++)
			{
				b[i][j]=0;
				a[i][j]=0;
				c[i][j]=0;
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d",&a[i][j]);
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d", &b[i][j]);
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				for(k=0;k<3;k++)
				{
					c[i][j] =c[i][j] +(a[i][j]*b[i][j]);
				}
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				printf("%d\t", c[i][j]);
			}
		printf("\n");
		}
		printf("\nis the multiplication of the matrix.\nThank you!");
		break;
	}

	case 4:
	{
	printf("Enter two martrix\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<3;j++)
			{
			b[i][j]=0;
			a[i][j]=0;
			c[i][j]=0;
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d",&a[i][j]);
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				scanf("%d", &b[i][j]);
			}
		}

		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
			{
				if(i>j)
				{
					k=a[i][j]+a[j][i];
					a[i][j]=k-a[i][j];
					a[j][i]=k-a[j][i];
				}
			}
		}
		for(i=0;i<3;i++)
		{
			for(j=0;j<3;j++)
				{
					printf("%d\t", c[i][j]);
				}
			printf("\n");
		}
		printf("\nis the transpoce of the matrix.\nThank you!");
		break;
		}
	case 5:
		{
		printf("Enter two martrix\n");
			for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
					{
					b[i][j]=0;
					a[i][j]=0;
					c[i][j]=0;
					}
			}
		for(i=0;i<3;i++)
			{
			for(j=0;j<3;j++)
				{
					scanf("%d",&a[i][j]);
				}
		}
		for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
				{
				scanf("%d", &b[i][j]);
				}
			}
		for(i=0;i<3;i++)
			{
			for(j=0;j<3;j++)
				{
					c[i][j] = a[i][j]-b[i][j];
				}
			}
		for(i=0;i<3;i++)
			{
				for(j=0;j<3;j++)
				{
					printf("%d\t", c[i][j]);
				}
				printf("\n");
		}
		printf("\nis the difference of the matrix.\nThank you!");
				break;
		}

	case 6:
		{
				printf("Enter two martrix\n");
				for(i=0;i<3;i++)
					{
					for(j=0;j<3;j++)
						{
						b[i][j]=0;
						a[i][j]=0;
						c[i][j]=0;
						}
					}
				for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								scanf("%d",&a[i][j]);
							}
						}
						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								scanf("%d", &b[i][j]);
							}
						}

						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								c[i][j] = a[i][j]-b[i][j];
							}
						}
						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								printf("%d\t", c[i][j]);
							}
							printf("\n");
						}
						printf("\nis the difference of the matrix.\nThank you!");
						break;
					}
	case 7:
			{
				printf("Enter two martrix\n");
				for(i=0;i<3;i++)
					{
					for(j=0;j<3;j++)
						{
						b[i][j]=0;
						a[i][j]=0;
						c[i][j]=0;
						}
					}
				for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								scanf("%d",&a[i][j]);
							}
						}
						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								scanf("%d", &b[i][j]);
							}
						}

						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								c[i][j] = a[i][j]-b[i][j];
							}
						}
						for(i=0;i<3;i++)
						{
							for(j=0;j<3;j++)
							{
								printf("%d\t", c[i][j]);
							}
							printf("\n");
						}
						printf("\nis the difference of the matrix.\nThank you!");
						break;
					}
	default:
		printf("Exit");
		break;
	}

}

