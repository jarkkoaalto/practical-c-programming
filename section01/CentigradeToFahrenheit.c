/*
 ============================================================================
 Name        : CentigradeToFahrenheit.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Centigrade To Fahrenheit program
Write to Program to convert temperature from degree centigrade to fahrenheit
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>


int main()
{
	float c,f;

	printf("Enter temp in centigrade:\n");
	scanf("%f", &c);

	f = (1.8 * c) + 32;
	printf("temp in fahrenheit = %f",f);
}


