/*
 ============================================================================
 Name        : FactorialNumber.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Factorial Number program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>


int main(){

	int n, i, fact=1;

	printf("Enter any number:");
	scanf("%d", &n);
	for(i=n; i>=1; i--){
		fact=fact*i;
	}
	printf("Factorial = %d", fact);

	return 0;
}



