/*
 ============================================================================
 Name        : StringsReverse.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Strings Reverse program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>
#include<string.h>

int main()
{

	int i,n;
	char str[20];

	printf("Enter string to get revesed:\n");
	gets(str);

	n = strlen(str);
	printf("\nReserved string is\n");

	for(i= n - 1 ;i >= 0; i--)
	{
		printf("%c", str[i]);
	}

	return 0;
}


