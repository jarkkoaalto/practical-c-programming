/*
 ============================================================================
 Name        : PrimeOrNot.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Prime Or Not program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>


int main(){

	int i,n,r=0;
	printf("Enter a positive integer");
	scanf("%d", &n);

	for(i=2;i<=n/2;++i)
	{
		if(n%i == 0)
		{
			r = 1;
			break;
		}
	}

	if(n == 1)
	{
		printf("\n1 is neither a prime nor a composite number.");
	}
	else
	{
		if(r == 0)
			printf("\n%d is a prime number.",n);
		else
			printf("\n%d is not a prime number", n);
	}

	return 0;
}




