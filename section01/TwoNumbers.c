/*
 ============================================================================
 Name        : TwoNumbers.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Add two numbers using pointers program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int *p1, *p2, sum;
	printf("Enter two number's:");
	scanf("%d %d",&*p1,&*p2);
	sum = *p1+*p2;
	printf("Sum = %d", sum);
	return 0;
}

