/*
 ============================================================================
 Name        : LeapYearOrNot.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Leap Year Or Not program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{

	int n;
	printf("Enter the year:");
	scanf("%d", &n);
	if(n % 4 == 0)
		printf("\nYear is Leap year");
	else
		printf("\nYear is not Leap year");
	return 0;
}

