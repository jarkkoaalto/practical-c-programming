/*
 ============================================================================
 Name        : DisplayMatrix.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Display Matrix program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int a[3][2];
	int b[3][2];
	int i,j;

	printf("Enter value for a matrix:");
	for(i=0;i<3;i++)
	{
		for(j=0;j<2;j++){
			scanf("%d",&a[i][j]);
		}
	}
	printf("Enter value for b matrix:");
	for(i=0;i<3;i++)
	{
		for(j=0;j<2;j++){
			scanf("%d",&b[i][j]);
		}
	}
	printf("\n a matrix is\n\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<2;j++)
		{
			printf("%d",a[i][j]);
		}
		printf("\n");
	}
	printf("\n b matrix is \n\n");
	for(i=0;i<3;i++)
	{
		for(j=0;j<2;j++)
		{
			printf("%d",b[i][j]);
		}
		printf("\n");
	}


	return 0;
}


