/*
 ============================================================================
 Name        : FindFactirial.c
 Author      : Jarkko Aalto
 Version     :
 Copyright   : Your copyright notice
 Description : Find Factirial program
 ============================================================================
*/
#include<stdio.h>
#include<conio.h>

int main()
{
	int n;
	int i;
	int fact = 1;

	printf("Enter any number:\n");
	scanf("%d", &n);

	for(i=n;i>=1;i--)
	{
		fact= fact*i;
	}
	printf("Factorial = %d", fact);

	return 0;
}

